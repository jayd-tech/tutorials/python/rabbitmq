import asyncio
import datetime as dt
import enum
import functools
import json
import logging
import os

import aio_pika
import pika
from aio_pika.abc import DeliveryMode
from aio_pika.pool import Pool
from pika.adapters.asyncio_connection import AsyncioConnection

LOGGER = logging.getLogger(__name__)
handler = logging.FileHandler("/logs/debug.log")
LOGGER.setLevel(logging.DEBUG)
LOGGER.addHandler(handler)

CREDENTIALS = pika.PlainCredentials(
    os.environ.get("RABBITMQ_USER"), os.environ.get("RABBITMQ_PASS")
)
CONNECT_PARAMS = pika.ConnectionParameters("rabbitmq", credentials=CREDENTIALS)


def get_rabbit_blocking_connection():
    return pika.BlockingConnection(CONNECT_PARAMS)


def get_rabbit_async_connection(
    on_open_callback, on_open_error_callback, on_close_callback
):

    connection = AsyncioConnection(
        CONNECT_PARAMS,
        on_open_callback=on_open_callback,
        on_open_error_callback=on_open_error_callback,
        on_close_callback=on_close_callback,
    )
    return connection


def get_aio_async_connection():
    return aio_pika.connect(
        host="rabbitmq",
        login=os.environ.get("RABBITMQ_USER"),
        password=os.environ.get("RABBITMQ_PASS"),
    )


class AsyncState(enum.Enum):
    BINDING_QUEUE = enum.auto()
    CONNECTING = enum.auto()
    CONNECTED = enum.auto()
    CONSUMING = enum.auto()
    DECLARING_EXCHANGE = enum.auto()
    DECLARING_QUEUES = enum.auto()
    DELETING_QUEUES = enum.auto()
    PUBLISHING = enum.auto()
    RECONNECTING = enum.auto()
    SETTING_QOS = enum.auto()
    STOPPED = enum.auto()
    STOPPING = enum.auto()
    READY = enum.auto()


class AioPika:
    def __init__(self):
        self._state = None

    @property
    def is_ready(self):
        return self._state is AsyncState.READY

    @property
    def is_stopping(self):
        return self._state is AsyncState.STOPPING

    @property
    def is_stopped(self):
        return self._state is AsyncState.STOPPED

    @property
    def state(self):
        return self._state

    async def _wait_until(self, wait_fn, error_fn, timeout=None):
        timer = 0
        while not wait_fn() or (timeout and timer >= timeout):
            timer += 1
            await asyncio.sleep(1)

        if timeout and timer >= timeout:
            error_fn()
        return

    async def _wait_until_state(self, *states, timeout=None):
        def wait_fn():
            return self._state in states

        def error_fn():
            LOGGER.error(
                "%s STOPPED WAITING FOR %s in %s. Timeout of %i exceeded",
                self,
                self._state,
                states,
                timeout,
            )

        return await self._wait_until(wait_fn, error_fn, timeout=timeout)

    async def _wait_until_property(self, property_name, timeout=None):
        def wait_fn():
            return getattr(self, property_name)

        def error_fn():
            LOGGER.error(
                "%s STOPPED WAITING FOR %s. Timeout of %i exceeded",
                self,
                property_name,
                timeout,
            )

        return await self._wait_until(wait_fn, error_fn, timeout=timeout)

    async def wait_until_ready(self, timeout=None):
        await self._wait_until_property("is_ready", timeout=timeout)

    def set_state(self, state):
        LOGGER.debug("Setting state of %s to %s", self, state)
        self._state = state


class AioConnectionPool(AioPika):
    def __init__(self, n_connections):
        super().__init__()
        self.pool = None
        asyncio.create_task(self.setup_pool(n_connections=n_connections))

    async def acquire(self):
        if self.is_ready:
            try:
                return self.pool.acquire()
            except:
                LOGGER.exception("POOL %s could not aquire channel", self)
        else:
            LOGGER.error(
                "POOL %s could not acquire channel. Not ready. Current state: %s",
                self,
                self._state,
            )

    async def close(self):
        LOGGER.info("Closing Pool: %s", self)
        self.set_state(AsyncState.STOPPING)
        try:
            await self.pool.close()
            LOGGER.debug("Closed Pool: %s", self)
            self.set_state(AsyncState.STOPPED)
        except:
            LOGGER.exception("COULD NOT CLOSE POOL: %s", self)

    @staticmethod
    async def get_connection():
        return await get_aio_async_connection()

    async def setup_pool(self, n_connections):
        LOGGER.debug("SETTING UP POOL for %s", self)
        self.pool = Pool(
            self.get_connection,
            max_size=n_connections,
            loop=asyncio.get_event_loop(),
        )
        LOGGER.debug("SET UP POOL FOR %s", self)
        self.set_state(AsyncState.READY)


class AioClient(AioPika):
    def __init__(self, exchange=None, client_name=None, pool=None):
        super().__init__()
        self.pool = pool
        self.client_name = client_name
        self._connection = None
        self._exchange_name = exchange
        self._exchange = None
        asyncio.create_task(self.connect())

    def __repr__(self):
        return self.client_name or super().__repr__()

    @property
    def exchange_name(self):
        return self._exchange_name or "default exchange" if self._exchange else None

    @property
    def is_connected(self):
        return self._state is AsyncState.CONNECTED or (
            self._connection
            and self._channel
            and not self.is_stopping
            or self.is_stopped
        )

    @property
    def is_consuming(self):
        return self._state is AsyncState.CONSUMING

    @property
    def is_ready(self):
        return super().is_ready or self.is_connected and self._exchange

    async def wait_until_connected(self, timeout=None):
        await self._wait_until_state(
            AsyncState.CONNECTED,
            AsyncState.READY,
            AsyncState.CONSUMING,
            timeout=timeout,
        )

    async def close(self):
        if not self.pool:
            LOGGER.debug("STOPPING: %s", self)
            self.set_state(AsyncState.STOPPING)
            await self._channel.close()
            await self._connection.close()
            self._channel = None
            self._connection = None
            self.set_state(AsyncState.STOPPED)
        else:
            LOGGER.error("COULD NOT CLOSE. %s is connected to a pool!", self)

    async def connect(self):
        LOGGER.info("Connecting %s", self)
        self.set_state(AsyncState.CONNECTING)
        if self.pool:
            try:
                LOGGER.debug("POOL: %s", self.pool.pool)
                async with self.pool.pool.acquire() as connection:
                    self._connection = connection
            except:
                LOGGER.exception(
                    "%s COULD NOT AQUIRE CONNECTION FROM POOL: %s", self, self.pool
                )
        else:
            self._connection = await get_aio_async_connection()

        try:
            LOGGER.debug("Declaring channel for %s", self)
            self._channel = await self._connection.channel()
            self.set_state(AsyncState.CONNECTED)
            if self._exchange_name is not None:
                if self._exchange_name != "":
                    self._exchange = await self.declare_exchange(self._exchange_name)
                else:
                    LOGGER.debug("Using default exchange for %s", self)
                    self._exchange = self._channel.default_exchange
                    self.set_state(AsyncState.READY)
        except:
            LOGGER.exception("COULD NOT DECLARE CHANNEL FOR %s", self)

    async def declare_exchange(
        self,
        name,
        type=aio_pika.ExchangeType.DIRECT,
        durable=False,
        auto_delete=False,
        timeout=None,
        **kwargs,
    ):
        if self.is_connected:
            LOGGER.debug("Declare exchange: %s, using: %s", name, self)
            return await self._channel.declare_exchange(
                name,
                type=type,
                durable=durable,
                auto_delete=auto_delete,
                timeout=timeout,
                **kwargs,
            )
        LOGGER.error(
            "CAN'T DECLARE EXCHANGE: %s. Not connected. Current state: %s",
            name,
            self._state,
        )

    async def declare_queue(
        self,
        name,
        durable=False,
        exclusive=False,
        auto_delete=False,
        timeout=None,
        **kwargs,
    ):
        ret = None
        if self.is_connected:
            LOGGER.debug("Declaring queue: %s, using: %s", name, self)
            self.set_state(AsyncState.DECLARING_QUEUES)
            ret = await self._channel.declare_queue(
                name,
                durable=durable,
                exclusive=exclusive,
                auto_delete=auto_delete,
                timeout=timeout,
                **kwargs,
            )
            self.set_state(AsyncState.READY if self.is_ready else AsyncState.CONNECTED)
            return ret

        LOGGER.error(
            "CAN'T DECLARE QUEUE: %s. Not connected. Current state: %s",
            name,
            self.state,
        )
        return ret

    async def delete_queue(
        self, queue_name, if_unused=True, if_empty=True, timeout=None
    ):
        ret = None
        if self.is_connected:
            try:
                queue = await self.get_queue(queue_name)
                LOGGER.debug("%s deleting queue: %s", self, queue_name)
                ret = await queue.delete(
                    if_unused=if_unused, if_empty=if_empty, timeout=timeout
                )
            except:
                LOGGER.exception("%s FAILED TO DELETE QUEUE: %s", self, queue_name)
        else:
            LOGGER.error(
                "%s COULD NOT DELETE QUEUE: %s. Not connected. State: %s",
                self,
                queue_name,
                self.state,
            )
        return ret


    async def get_queue(self, name):
        if self.is_ready:
            try:
                LOGGER.debug("%s is getting queue: %s", self, name)
                return await self._channel.get_queue(name)
            except:
                LOGGER.exception("%s could not get queue: %s", self, name)
        else:
            LOGGER.error(
                "%s could not get queue: %s. Not ready. Current state: %s",
                self,
                name,
                self._state,
            )

    async def publish_message(
        self,
        routing_key,
        message,
        headers=None,
        content_type=None,
        delivery_mode="persistent",
        correlation_id=None,
        reply_to=None,
        expiration=None,
        message_id=None,
        **properties,
    ):
        """
        Publish a message to RabbitMQ.

        Only publishes if class is connected.
        """
        ret = None
        if self.is_ready:
            previous_state = self._state
            try:
                if isinstance(message, dict):
                    message = json.dumps(message)
                if isinstance(message, str):
                    message = message.encode("utf-8")
                elif not isinstance(message, bytes):
                    LOGGER.error(
                        "%s COULD NOT PUBLISH TO exchange: %s with key: %s. Unknown message: %s, of type: %s",
                        self,
                        self.exchange_name,
                        routing_key,
                        message,
                        type(message),
                    )
                    return

                delivery_mode = getattr(DeliveryMode, delivery_mode.upper())
                self.set_state(AsyncState.PUBLISHING)
                ret = await self._exchange.publish(
                    aio_pika_message := aio_pika.Message(
                        message,
                        headers=headers,
                        content_type=content_type,
                        delivery_mode=delivery_mode,
                        correlation_id=correlation_id,
                        reply_to=reply_to,
                        expiration=expiration,
                        message_id=message_id,
                        **properties,
                    ),
                    routing_key=routing_key,
                )
                LOGGER.info(
                    "%s published message: %s, to %s with key: %s",
                    self,
                    message,
                    self.exchange_name,
                    routing_key,
                )
                self._message_number += 1
            except:
                LOGGER.exception("ERROR: COULD NOT PUBLISH MESSAGE")
            finally:
                self.set_state(previous_state)
        else:
            LOGGER.error(
                "%s COULD NOT PUBLISH TO exchange: %s with key: %s. Channel closed. Current state: %s",
                self,
                self.exchange_name,
                routing_key,
                self._state,
            )
        return ret
    async def set_prefetch_count(self, n):
        if self.is_connected or self.is_consuming:
            previous_state = self._state
            self.set_state(AsyncState.SETTING_QOS)
            ret = await self._channel.set_qos(prefetch_count=n, timeout=timeout)
            self.set_state(
                AsyncState.CONSUMING
                if previous_state == AsyncState.CONSUMING
                else AsyncState.READY
                if self.is_ready
                else AsyncState.CONNECTED
            )
            return ret
        else:
            LOGGER.error(
                "Can't set %s's prefetch_count. Not connected or  ready. Current state: %s",
                self,
                n,
            )

    async def start_consuming(
        self,
        queue_name,
        callback,
        no_ack=False,
        exclusive=False,
        timeout=None,
        **kwargs,
    ):
        LOGGER.debug(
            "%s is issuing consumer related RPC commands on queue: %s", self, queue_name
        )
        if self.is_ready:
            try:
                queue = await self.get_queue(queue_name)
                if queue:
                    ret = await queue.consume(
                        callback=callback,
                        no_ack=no_ack,
                        exclusive=exclusive,
                        consumer_tag=kwargs.get("consumer_tag"),
                        timeout=timeout,
                        arguments=kwargs,
                    )
                    LOGGER.info("%s is now consuming: %s", self, queue_name)
                    self.set_state(AsyncState.CONSUMING)
                    return ret
            except:
                LOGGER.exception(
                    "%s FAILED TO CONSUME FROM %s", self, self.exchange_name
                )
        else:
            LOGGER.error(
                "%s FAILED NOT CONSUME FROM %s. Not ready. Current state: %s",
                self,
                self.exchange_name,
                self.state,
            )
        return


class AsyncClient:
    def __init__(self, exchange):
        self._state = None
        self._exchange_name = exchange
        self._channel = None
        self._connection = self.connect()
        self._message_number = 0
        self._messages = {}
        self._message_states = {}

    @property
    async def is_ready(self):
        while not (self._state is AsyncState.CONNECTED and self._channel):
            await asyncio.sleep(1)

    async def wait_until_ready(self, timeout=float("inf")):
        time_waited = 0
        while not (self._state is AsyncState.CONNECTED and self._channel):
            if not time_waited > timeout:
                time_waited += 1
                await asyncio.sleep(1)
            else:
                LOGGER.error("TIMED OUT while waiting for client to be ready")
                self.stop()

    def redeliver(self):
        resends = []

        def is_resendable(message_id, info, now):
            _is_resendable = info["status"] == "nack" and now - info[
                "last_update"
            ] > dt.timedelta(minutes=1)
            if _is_resendable:
                resends.append(
                    {
                        "message": self._messages[message_id]["message"],
                        "routing_key": self._messages[message_id]["routing_key"],
                        "properties": self._messages[message_id]["properties"],
                    }
                )

        try:
            if any(
                is_resendable(message_id, info)
                for message_id, info in self._message_states.items()
            ):
                for resend in resends:
                    self.publish_message(
                        resend["routing_key"], resend["message"], resend["properties"]
                    )
        except:
            LOGGER.exception("ERROR, could not redeliver w/ resends: %s", resends)

        self._connection.ioloop.call_later(60, self.redeliver)

    def acknowledge_message(self, delivery_tag):
        if self._state is AsyncState.CONSUMING:
            LOGGER.debug("Acknowledging message %s", delivery_tag)
            self._channel.basic_ack(delivery_tag)
        else:
            LOGGER.error(
                "CANNOT ACKNOWEDGE MESSAGE. NOT CONSUMING. State: %s", self._state
            )

    def reject_message(self, delivery_tag):
        if self._state is AsyncState.CONSUMING:
            LOGGER.debug("Rejecting message %s", delivery_tag)
            self._channel.basic_nack(delivery_tag)
        else:
            LOGGER.error("CANNOT REJECT MESSAGE. NOT CONSUMING. State: %s", self._state)

    def connect(self):
        """
        Connect to RabbitMQ, returning connection handle. After connection is established, pika invokes
        on_open_callback method.
        """
        self._state = AsyncState.CONNECTING
        try:
            connection = get_rabbit_async_connection(
                self.on_connection_open,
                self.on_connection_open_error,
                self.on_connection_unexpectedly_closed,
            )
            LOGGER.debug("GOT CONNECTION: %s", connection)
            return connection
        except:
            LOGGER.exception("CANT CONNECT")

    def on_connection_open(self, _unused_connection):
        """
        Call once pika successfully connects to RabbitMQ
        """
        LOGGER.debug("CONNECTION OPENED")
        self.open_channel()

    def on_connection_open_error(self, _unused_connection, err):
        """
        Call if pika fails to establish connection ro RabbitMQ
        """
        LOGGER.error("Connection failed, reopening in 5 seconds: %s", err)
        self._state = AsyncState.RECONNECTING
        self._connection.ioloop.call_later(5, self._connection.ioloop.stop)

    def on_connection_unexpectedly_closed(self, _unused_connection, reason):
        """
        Try to reconnect on unexpected RabbitMq disconnect
        """
        LOGGER.debug("HERE")
        self._channel = None
        if self._state is AsyncState.STOPPING:
            self._connection.ioloop.stop()
        else:
            LOGGER.debug(
                "CONNECTION unexpectedly closed, reopening in 5 seconds: %s", reason
            )
            self._state = AsyncState.RECONNECTING
            self._connection.ioloop.call_later(5, self._connection.ioloop.stop)

    def open_channel(self):
        """
        Open channel by issuing Channel.Open RPC command. RabbitMQ confirms channel is open via
        Channel.OpenOK RPC reply.
        """
        LOGGER.debug("Creating a new channel")
        self._channel = self._connection.channel(on_open_callback=self.on_channel_open)
        self._state = AsyncState.CONNECTED
        self.redeliver()

    def on_channel_open(self, channel):
        """
        Invoke via pika after opening channel, and declare exchange to use
        """
        LOGGER.debug("Channel opened")
        self._channel = channel
        self._channel.add_on_close_callback(self.on_channel_unexpectedly_closed)
        self.declare_exchange(self._exchange_name)

    def on_channel_unexpectedly_closed(self, channel, reason):
        """
        Pika calls when RabbitMQ unexpectedly closes channel (usually when you try and violated protocol - re-declare exchange/queue w/ different parameters)
        """
        LOGGER.warning("Channel %i was closed: %s", channel, reason)
        self._channel = None
        if not self._state is AsyncState.STOPPING:
            self._connection.close()

    def declare_exchange(self, exchange_name):
        """
        Invoke Exchange.Declare RPC command in order to setup exchange on RabitMQ. Pika calls on_exchange_declareok_callback
        afterwards.
        """
        LOGGER.debug("Declaring exchange %s", exchange_name)

        if exchange_name == "":
            return

        cb = functools.partial(self.on_exchange_declareok, userdata=exchange_name)
        self._exchange_name = exchange_name
        self._state = AsyncState.DECLARING_EXCHANGE
        self._channel.exchange_declare(exchange=exchange_name, callback=cb)

    def on_exchange_declareok(self, _unusued_frame, userdata):
        LOGGER.debug("Exchange declared: %s", userdata)
        self._state = AsyncState.CONNECTED

    def declare_queues(self, queue_names, queue_names_to_binding_keys=None):
        """
        Declare queue on RabbitMQ via Queue.Declare RPC command. On complete, pika invokes
        on_queue_declareok method.
        """
        if self._state is AsyncState.CONNECTED:
            self._state = AsyncState.DECLARING_QUEUES
            for queue_name in queue_names:
                cb = functools.partial(
                    self.on_queue_declareok,
                    queue_name=queue_name,
                    binding_keys=(queue_names_to_binding_keys or {}).get(
                        queue_name, []
                    ),
                )
                LOGGER.debug("DECLARING QUEUE: %s", queue_name)
                self._channel.queue_declare(queue=queue_name, callback=cb)
        else:
            LOGGER.error(
                "Could not declare queues: %s. Not connected. State: %s",
                queue_names,
                self._state,
            )

    def delete_queues(self, queue_names):
        if self._state == AsyncState.CONNECTED:
            self._state = AsyncState.DELETING_QUEUES
            for queue_name in queue_names:
                LOGGER.debug("DELETING QUEUE: %s", queue_name)
                self._channel.queue_delete(queue=queue_name)
            self._state = AsyncState.CONNECTED
        else:
            LOGGER.error(
                "Could not delete queues: %s. Not connected. State: %s",
                queue_name,
                self._state,
            )

    def on_queue_declareok(self, _unused_frame, queue_name, binding_keys):
        """
        Pika will invoke after Queue.Declare RPC call(s) from declare_queues has/have completed.
        Declared queues are bound (if necessary) to mapped routing keys. Afterwards, pika will invoke
        on_bindok
        """
        if binding_keys:
            for i, binding_key in enumerate(binding_keys):
                kwargs = {"binding_key": binding_key}
                if i == len(binding_keys) - 1:
                    kwargs["change_state_to_connected":True]

                cb = functools.partial(self.on_bindok, **kwargs)
                LOGGER.debug(
                    "BINDING %s to %s with routing_key: %s",
                    self.exchange_name,
                    queue_name,
                    binding_key,
                )
                self._state = AsyncState.BINDING_QUEUE
                self._channel.queue_bind(
                    queue_name, self.exchange_name, routing_key=binding_key, callback=cb
                )
        else:
            self._state = AsyncState.CONNECTED

    def on_bindok(self, _unused_frame, binding_key, change_state_to_connected=False):
        """
        Pika will invoke after receiving Queue.BindOk from RabbitMQ
        """
        LOGGER.debug("Queue bound w/ key: %s", binding_key)
        if change_state_to_connected:
            self._state = AsyncState.CONNECTED

    def enable_delivery_confirmations(self):
        """
        Send Confirm.Select RPC method to RabbitMQ to enable delivery confirmations on channel. On confirmation,
        on_delivery_confirmation method invoked. This will pass in a Basic.Ack or Basic.Nack method from RabbitMQ.
        """
        LOGGER.debug("Issuing Confirm.Select RPC command")
        self._channel.confirm_delivery(self.on_delivery_confirmation)

    def on_delivery_confirmation(self, method_frame):
        """
        Pika will invoke when RabbitMQ responds to a Basic.Publish RPC command w/ either Basic.Ack or Basic.Nack
        frame and delivery tag of the message that was published.
        """
        confirmation_type = method_frame.method.NAME.split(".")[1].lower()
        ack_multiple = method_frame.method.multiple
        delivery_tag = method_frame.method.delivery_tag

        LOGGER.debug(
            "Received %s for delivery tag: %i (multiple: %s)",
            confirmation_type,
            delivery_tag,
            ack_multiple,
        )
        if ack_multiple:
            for tmp_tag in self._messages.keys():
                if tmp_tag <= delivery_tag:
                    self._message_states[tmp_tag] = delivery_tag
                    del self._messages[tmp_tag]
        else:
            self._message_states[delivery_tag] = confirmation_type
            del self._messages[delivery_tag]

    def publish_message(self, routing_key, message, properties=None):
        """
        Publish a message to RabbitMQ.

        Only publishes if class is connected.
        """
        if self._state in (AsyncState.CONNECTED, AsyncState.CONSUMING):
            if self._channel.is_open:
                previous_state = self._state
                self._state = AsyncState.PUBLISHING
                try:
                    properties = pika.BasicProperties(**(properties or {}))
                    self._channel.basic_publish(
                        exchange=self._exchange_name,
                        routing_key=routing_key,
                        body=message,
                        properties=properties,
                    )
                    self._message_number += 1
                    self._messages[self._message_number] = {
                        "message": message,
                        "routing_key": routing_key,
                        "properties": properties,
                    }
                    LOGGER.debug("Published message # %i", self._message_number)
                except:
                    LOGGER.exception("ERROR: COULD NOT PUBLISH MESSAGE")
                finally:
                    self._state = previous_state
            else:
                LOGGER.error(
                    "COULD NOT PUBLISH TO exchange: %s with key: %s. Channel closed",
                    self._exchange_name,
                    routing_key,
                )
        else:
            LOGGER.error(
                "COULD NOT PUBLISH TO exchange: %s with key % s. State is %s",
                self._exchange_name,
                routing_key,
                self._state,
            )

    async def set_qos(self, prefetch_count):
        await self.wait_until_ready(timeout=10)
        cb = functools.partial(self.on_basic_qos_ok, prefetch_count=prefetch_count)
        self._channel.basic_qos(prefetch_count=prefetch_count, callback=cb)

    def start_consuming(self, queue_name, callback):
        """
        Set up the client for consumption.

        Handles when RabbitMQ cancels the consumer by closing the channel.
        """
        LOGGER.debug("Issuing consumer related RPC commands")
        if self._state is AsyncState.CONNECTED:
            LOGGER.debug("Adding consumer cancellation callback")
            self._channel.add_on_cancel_callback(self.on_consumer_cancelled)
            self._consumer_tag = self._channel.basic_consume(queue_name, callback)
            self._state = AsyncState.CONSUMING
        else:
            logging.warning("Could not start consuming. Client is not connected")

    def on_consumer_cancelled(self, method_frame):
        LOGGER.warning(
            "Consumer was cancelled remotely, shutting down: %r", method_frame
        )
        self.stop()

    def on_basic_qos_ok(self, _unused_frame, prefetch_count):
        LOGGER.debug("QOS set to %i", prefetch_count)

    def stop(self):
        LOGGER.debug("STOPPING")
        self._state = AsyncState.STOPPING
        self.close_channel()

    def close_channel(self):
        if self._channel:
            LOGGER.debug("Closing channel")
            self._channel.close()

    def close_connection(self):
        if self._connection:
            LOGGER.debug("Closing connection")
            self._connection.close()
        self._state = AsyncState.STOPPED
