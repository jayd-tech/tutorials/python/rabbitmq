import random
import uuid

import pytest


@pytest.fixture
def rpc_infos():
    def get_random_math_params():
        operation = random.choice(("+", "-", "*"))
        return {
            "int_1": random.randint(1, 100),
            "int_2": random.randint(1, 100),
            "str_op": operation,
        }

    target_info, dummy_info = {}, {}
    for info in (target_info, dummy_info):
        info["correlation_ids"] = [str(uuid.uuid4()) for _ in range(2)]
        info["random_math_params"] = [get_random_math_params() for _ in range(2)]
        info["callback_queue_name"] = str(uuid.uuid4())
    return {"target_info": target_info, "dummy_info": dummy_info}
