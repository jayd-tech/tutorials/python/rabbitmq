import io
import json
import threading
import pika
from tenacity import retry, stop_after_attempt, wait_fixed

from tutorial.client import get_rabbit_blocking_connection


@retry(reraise=True, stop=stop_after_attempt(5), wait=wait_fixed(1))
def assert_confirmation(fn):
    fn()


def test_send_hello():
    """Send message to queue"""
    in_memory_files = {}
    access_files_lock = threading.Lock()

    def confirm():
        with access_files_lock:
            assert (
                in_memory_files["confirm_hello.txt"].getvalue() == "How are ya, buddy"
            )

    def publish_msg():
        get_rabbit_blocking_connection().channel().basic_publish(
            exchange="", routing_key="hello", body="Hello Word!"
        )

    def receive_msg():
        def callback(ch, method, properties, body):
            with access_files_lock:
                in_memory_files["confirm_hello.txt"] = (f := io.StringIO())
                f.write("How are ya, buddy")

        (channel := get_rabbit_blocking_connection().channel()).basic_consume(
            queue="hello", on_message_callback=callback, auto_ack=True
        )
        channel.start_consuming()

    (channel := get_rabbit_blocking_connection().channel()).queue_declare(queue="hello")
    threading.Thread(target=receive_msg, daemon=True).start()
    publish_msg()
    try:
        assert_confirmation(confirm)
    finally:
        channel.queue_delete(queue="hello")


def test_send_hello_json():
    in_memory_files = {}
    access_files_lock = threading.Lock()

    def confirm():
        with access_files_lock:
            assert in_memory_files["confirm_json_hello.txt"].getvalue() == "Hello World"

    def publish_msg():
        get_rabbit_blocking_connection().channel().basic_publish(
            exchange="",
            routing_key="json-hello",
            properties=pika.BasicProperties(content_type="application/json"),
            body=json.dumps({"message": "Hello World"}),
        )

    def receive_msg():
        def callback(ch, method, properties, body):
            message_info = json.loads(body)
            with access_files_lock:
                in_memory_files["confirm_json_hello.txt"] = (f := io.StringIO())
                f.write(message_info["message"])

        (channel := get_rabbit_blocking_connection().channel()).basic_consume(
            queue="json-hello", on_message_callback=callback, auto_ack=True
        )
        channel.start_consuming()

    (channel := get_rabbit_blocking_connection().channel()).queue_declare(
        queue="json-hello"
    )
    threading.Thread(target=receive_msg, daemon=True).start()
    publish_msg()

    try:
        assert_confirmation(confirm)
    finally:
        channel.queue_delete(queue="json-hello")
