import io
import threading

import pytest
from tenacity import retry, stop_after_attempt, wait_fixed

from tutorial.client import get_rabbit_blocking_connection


def test_ack():
    """Send message to queue"""
    in_memory_files = {"confirm.txt": io.StringIO()}
    access_files_lock = threading.Lock()

    @retry(reraise=True, stop=stop_after_attempt(3), wait=wait_fixed(1))
    def assert_confirmation_txt(txt):
        with access_files_lock:
            assert in_memory_files["confirm.txt"].getvalue() == txt

    def publish_msg():
        get_rabbit_blocking_connection().channel().basic_publish(
            exchange="", routing_key="do", body="do add!"
        )

    def try_and_fail():
        def callback(ch, method, properties, body):
            if body.decode("utf-8") == "do add!":
                with access_files_lock:
                    in_memory_files["confirm.txt"]  # Access but don't write
                    ch.close()  # fail while writing

        (channel := get_rabbit_blocking_connection().channel()).basic_consume(
            queue="do", on_message_callback=callback
        )
        channel.basic_qos(prefetch_count=1)
        channel.start_consuming()

    def try_and_ack():
        def callback(ch, method, properties, body):
            if body.decode("utf-8") == "do add!":
                with access_files_lock:
                    in_memory_files["confirm.txt"].write("1 + 1 = 2")

                ch.basic_ack(delivery_tag=method.delivery_tag)

        (channel := get_rabbit_blocking_connection().channel()).basic_consume(
            queue="do", on_message_callback=callback
        )
        channel.basic_qos(prefetch_count=1)
        channel.start_consuming()

    (channel := get_rabbit_blocking_connection().channel()).queue_declare(
        queue="do", durable=True
    )
    threading.Thread(target=try_and_fail, daemon=True).start()
    publish_msg()
    with pytest.raises(AssertionError):
        try:
            assert_confirmation_txt("1 + 1 = 2")
        except:
            raise
    # message should remain in queue due to failure to ack
    threading.Thread(target=try_and_ack, daemon=True).start()

    try:
        assert_confirmation_txt("1 + 1 = 2")
    finally:
        channel.queue_delete("do")
