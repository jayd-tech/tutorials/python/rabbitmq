import io
import os.path
import threading
from multiprocessing.pool import ThreadPool

from tenacity import retry, stop_after_attempt, wait_fixed

from tutorial.client import get_rabbit_blocking_connection


class TestLogs:
    @staticmethod
    def assert_no_stale_files(file_names):
        assert not any(dir_entry.name in file_names for dir_entry in os.scandir("."))

    @staticmethod
    @retry(reraise=True, stop=stop_after_attempt(3), wait=wait_fixed(2))
    def repeated_assert_fn(assert_fn):
        assert_fn()

    def test_multi_logs(self):
        """With fanout, multiple subscribers log from a single producer"""

        in_memory_files = {}
        access_files_lock = threading.Lock()

        def publish_fn():
            get_rabbit_blocking_connection().channel().basic_publish(
                exchange="logs",
                routing_key="",
                body="Are you listening? Woah-o-o-ooo",
            )

        def assert_fn(publish_fn):
            try:
                for i in range(1, 4):
                    with access_files_lock:
                        assert (
                            "Are you listening"
                            in in_memory_files[f"subscriber_{i}.log"].getvalue()
                        )
            except:
                publish_fn()  # Consumer may have been started too late
                raise

        def bind_exchange_to_random_queue(channel):
            target_queue = channel.queue_declare(
                queue="", exclusive=True
            )  # any random queue will self destruct when we're done
            channel.queue_bind(
                exchange="logs", queue=(queue_name := target_queue.method.queue)
            )
            return queue_name

        def log_it_up(name):
            def callback(ch, method, properties, body):
                with access_files_lock:
                    in_memory_files[f"{name}.log"] = (f := io.StringIO())
                    f.write(f"Message consumed by {name}: {body.decode('utf-8')} \n")
                    f.write(f"Sing it back, woah-o-o-o-o")

            channel = get_rabbit_blocking_connection().channel()
            queue_name = bind_exchange_to_random_queue(channel)
            channel.basic_consume(
                queue=queue_name, on_message_callback=callback, auto_ack=True
            )
            channel.start_consuming()

        connection = get_rabbit_blocking_connection()
        channel = connection.channel()
        channel.exchange_declare(exchange="logs", exchange_type="fanout")
        pool = ThreadPool(processes=3)
        pool.map_async(log_it_up, [f"subscriber_{i}" for i in range(1, 4)])

        try:
            publish_fn()
            self.repeated_assert_fn(lambda: assert_fn(publish_fn))
        except:
            raise
        finally:
            pool.close()
            channel.exchange_delete("logs")

    def test_selective_logging(self):
        in_memory_files = {}
        access_files_lock = threading.Lock()

        def assert_fn(publish_fn):
            try:
                for i in range(1, 3):
                    f = in_memory_files[f"subscriber_{i}.log"]
                    if i == 1:
                        assert "Just a warning" in (content := f.getvalue())
                    assert "This is an EMERGENCY" in content
            except:
                publish_fn()  # Consumer may have been started too late
                raise

        def bind_exchange_to_random_queue(channel, *severities):
            for severity in severities:
                target_queue = channel.queue_declare(queue="", exclusive=True)
                queue_name = target_queue.method.queue
                for severity in severities:
                    channel.queue_bind(
                        exchange="logs",
                        queue=queue_name,
                        routing_key=severity,
                    )
            return queue_name

        def publish_fn():
            (channel := get_rabbit_blocking_connection().channel()).basic_publish(
                exchange="logs", routing_key="warning", body="Just a warning"
            )
            channel.basic_publish(
                exchange="logs", routing_key="error", body="This is an EMERGENCY"
            )

        def log_it_up(name_and_severities):
            name, severities = name_and_severities[0], name_and_severities[1]

            def callback(ch, method, properties, body):
                with access_files_lock:
                    if in_memory_files.get(f"{name}.log"):
                        f = in_memory_files[f"{name}.log"]
                        f.seek(0, io.SEEK_END)
                    else:
                        in_memory_files[f"{name}.log"] = (f := io.StringIO())
                    f.write(f"{body.decode('utf-8')} \n")

            channel = get_rabbit_blocking_connection().channel()
            queue_name = bind_exchange_to_random_queue(channel, *severities)
            channel.basic_consume(
                queue=queue_name, on_message_callback=callback, auto_ack=True
            )
            channel.start_consuming()

        connection = get_rabbit_blocking_connection()
        channel = connection.channel()
        channel.exchange_declare(
            exchange="logs", exchange_type="direct", auto_delete=True
        )
        pool = ThreadPool(processes=2)
        pool.map_async(
            log_it_up,
            (
                ("subscriber_1", ("warning", "error")),
                ("subscriber_2", ("error",)),
            ),
        )

        try:
            publish_fn()
            self.repeated_assert_fn(lambda: assert_fn(publish_fn))
        except:
            raise
        finally:
            pool.close()
            channel.exchange_delete("logs")

    def test_topic_exchange(self):
        FRENCH_SALUATION = "Est-ce que tu parles français"
        INSIDE_JOKE = "But I'm not a violet"
        NY_SALUATION = "Welcome to NY"
        STRONG_RED_FOX = "STRONG RED FOX STRONG"
        in_memory_files = {}
        access_files_lock = threading.Lock()

        def assert_fn(publish_fn):
            try:
                with access_files_lock:
                    f = in_memory_files[f"subscriber.log"]
                    assert INSIDE_JOKE in (content := f.getvalue())
                    assert FRENCH_SALUATION in content
                    assert NY_SALUATION in content
                    assert not STRONG_RED_FOX in content
            except:
                publish_fn()  # Consumer may have been started too late
                raise

        def publish_fn():
            channel = get_rabbit_blocking_connection().channel()
            channel.basic_publish(
                exchange="logs",
                routing_key="pretentious.purple.flower",
                body=INSIDE_JOKE,
            )
            channel.basic_publish(
                exchange="logs",
                routing_key="strong.red.fox.strong",
                body=STRONG_RED_FOX,
            )
            channel.basic_publish(
                exchange="logs",
                routing_key="welcome.to.ny",
                body=NY_SALUATION,
            )
            channel.basic_publish(
                exchange="logs",
                routing_key="welcome.to.france",
                body=FRENCH_SALUATION,
            )

        def log_it_up():
            def callback(ch, method, properties, body):
                with access_files_lock:
                    if in_memory_files.get("subscriber.log"):
                        f = in_memory_files["subscriber.log"]
                        f.seek(0, io.SEEK_END)
                    else:
                        in_memory_files["subscriber.log"] = (f := io.StringIO())
                    f.write(f"{body.decode('utf-8')} \n")

            channel = get_rabbit_blocking_connection().channel()
            target_queue = channel.queue_declare(queue="", exclusive=True)
            queue_name = target_queue.method.queue
            for binding_key in ("welcome.#", "*.*.*"):
                channel.queue_bind(
                    exchange="logs",
                    queue=queue_name,
                    routing_key=binding_key,
                )

            channel.basic_consume(
                queue=queue_name, on_message_callback=callback, auto_ack=True
            )
            channel.start_consuming()

        connection = get_rabbit_blocking_connection()
        channel = connection.channel()
        channel.exchange_declare(
            exchange="logs", exchange_type="topic", auto_delete=True
        )
        threading.Thread(target=log_it_up, daemon=True).start()

        try:
            publish_fn()
            self.repeated_assert_fn(lambda: assert_fn(publish_fn))
        except:
            raise
        finally:
            channel.exchange_delete("logs")
