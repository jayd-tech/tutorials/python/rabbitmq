import asyncio
import io
import json
import logging
import threading

import pytest
from aio_pika import connect
from tenacity import retry, stop_after_attempt, wait_fixed

from tutorial.client import AioClient, AioConnectionPool, AsyncState

LOG_FORMAT = (
    "%(levelname) -10s %(asctime)s %(name) -30s %(funcName) "
    "-35s %(lineno) -5d: %(message)s"
)

LOGGER = logging.getLogger(__name__)
handler = logging.FileHandler("/logs/debug.log", mode="w")
LOGGER.setLevel(logging.DEBUG)
LOGGER.addHandler(handler)


CLIENT_ACTION_TIMEOUT = 5


@pytest.mark.asyncio
async def test_aioclient_ready():
    client = AioClient(exchange="", client_name="Starter")
    await client.wait_until_ready()
    assert client.state == AsyncState.READY
    await client.close()


@pytest.mark.asyncio
async def test_connection_pool():
    """
    Confirm client can connect using pool
    """
    pool = AioConnectionPool(n_connections=1)
    await pool.wait_until_ready(5)
    client = AioClient(exchange="", pool=pool)
    await client.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
    await pool.close()


@pytest.mark.asyncio
async def test_simple_produce_consume():
    in_memory_files = {"test.txt": io.StringIO()}

    @retry(reraise=True, stop=stop_after_attempt(3), wait=wait_fixed(5))
    async def assert_with_retry():
        assert in_memory_files["test.txt"].getvalue() == "Hello world"

    async def produce(pool):
        LOGGER.debug("RUNNING")
        publisher = AioClient(exchange="", client_name="PRODUCER", pool=pool)
        await publisher.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
        await publisher.declare_queue("hello", timeout=CLIENT_ACTION_TIMEOUT)
        await publisher.publish_message(
            "hello",
            message="Hello world",
            content_type="text/plain",
            delivery_mode="not_persistent",
        )

    async def consume(pool):
        consumer = AioClient(exchange="", client_name="CONSUME", pool=pool)

        async def callback(message):
            async with message.process():
                if message.content_type == "text/plain":
                    message_body = message.body.decode("utf-8")
                    in_memory_files["test.txt"].write(message_body)
                else:
                    await message.reject()

        await consumer.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
        await consumer.declare_queue("hello", timeout=CLIENT_ACTION_TIMEOUT)
        await consumer.start_consuming("hello", callback)

    pool = AioConnectionPool(3)
    await pool.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
    client = AioClient(exchange="", client_name="Starter", pool=pool)
    await client.wait_until_ready()
    asyncio.create_task(produce(pool))
    asyncio.create_task(consume(pool))
    try:
        await assert_with_retry()
    finally:
        await client.delete_queue("hello", timeout=CLIENT_ACTION_TIMEOUT)
        await pool.close()


@pytest.mark.asyncio
async def test_delete_queue():
    in_memory_files = {}
    access_files_lock = threading.Lock()

    @retry(reraise=True, stop=stop_after_attempt(3), wait=wait_fixed(5))
    async def assert_w_retry():
        with access_files_lock:
            assert "First" not in (content := in_memory_files["test.txt"].getvalue())
            assert "Second" in content

    async def consume(pool):
        consumer = AioClient(exchange="", client_name="Consumer", pool=pool)

        async def callback(message):
            async with message.process():
                in_memory_files["test.txt"] = (f := io.StringIO())
                f.write(message.body.decode("utf-8"))

        await consumer.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
        await consumer.declare_queue(
            "test", durable=True, timeout=CLIENT_ACTION_TIMEOUT
        )
        await consumer.start_consuming("test", callback, timeout=CLIENT_ACTION_TIMEOUT)

    async def produce(message, client_name, pool):
        producer = AioClient(exchange="", client_name=client_name, pool=pool)

        await producer.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
        await producer.declare_queue(
            "test", durable=True, timeout=CLIENT_ACTION_TIMEOUT
        )
        await producer.publish_message("test", message, delivery_mode="not_persistent")

    pool = AioConnectionPool(4)
    await pool.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
    starter = AioClient(exchange="", client_name="Starter", pool=pool)
    await starter.wait_until_connected(timeout=CLIENT_ACTION_TIMEOUT)
    await produce("First", "Initial Producer", pool)
    await starter.delete_queue(
        "test", if_unused=False, if_empty=False, timeout=CLIENT_ACTION_TIMEOUT
    )
    asyncio.create_task(consume(pool))
    await produce("Second", "Second Producer", pool)

    try:
        await assert_w_retry()
    finally:
        await starter.delete_queue("test", if_unused=False, if_empty=False)
        await pool.close()


@pytest.mark.asyncio
async def test_random_math_rpc(rpc_infos):
    in_memory_files = {}
    access_files_lock = threading.Lock()

    def do_random_math(int_1, int_2, str_op):
        return eval(f"{int_1}{str_op}{int_2}")

    async def assert_fn_with_retry(lookup_info):
        @retry(reraise=True, stop=stop_after_attempt(3), wait=wait_fixed(5))
        async def assert_w_retry():
            with access_files_lock:
                for i, correlation_id in enumerate(lookup_info["correlation_ids"]):
                    content = in_memory_files[f"rpc_{correlation_id}.txt"].getvalue()
                assert content == str(
                    do_random_math(**lookup_info["random_math_params"][i])
                )

        await assert_w_retry()

    async def client_consume(correlation_id, callback_queue_name, pool):
        consumer = AioClient(
            exchange="", client_name=f"CLIENT_CONSUMER_{correlation_id}", pool=pool
        )

        async def callback(message):
            async with message.process():
                if correlation_id == message.correlation_id:
                    in_memory_files[f"rpc_{correlation_id}.txt"] = (f := io.StringIO())
                    with access_files_lock:
                        f.write(message.body.decode("utf-8"))
                    LOGGER.debug(
                        "%s Wrote: %s to: %s",
                        consumer,
                        message.body.decode("utf-8"),
                        f"rpc_{correlation_id}.txt",
                    )
                else:
                    LOGGER.debug("%s is rejecting message", consumer)
                    await message.reject(requeue=True)

        await consumer.wait_until_connected(timeout=CLIENT_ACTION_TIMEOUT)
        await consumer.set_prefetch_count(1, timeout=CLIENT_ACTION_TIMEOUT)
        await consumer.declare_queue(callback_queue_name)
        await consumer.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
        await consumer.start_consuming(callback_queue_name, callback)

    async def server_consume(pool):
        consumer = AioClient(exchange="", client_name="SERVER_CONSUME", pool=pool)

        async def callback(message):
            async with message.process():
                if message.content_type == "application/json":
                    LOGGER.debug("SERVER GOT MESSAGE: %s", message.info())
                    message_body = json.loads(message.body)
                    ret = do_random_math(**message_body)
                    delivered_message = await consumer.publish_message(
                        routing_key=message.reply_to,
                        message=str(ret),
                        correlation_id=message.correlation_id,
                    )
                    if not delivered_message:
                        LOGGER.warning(
                            "MESSAGE: %s, NOT DELIVERED. %s IS DROPPING...",
                            message_body,
                            consumer,
                        )
                        await message.reject()
                else:
                    LOGGER.warning(
                        "MESSAGE: %s, HAS WRONG content_type: %s. %s IS DROPPING...",
                        message_body,
                        message.content_type,
                        consumer,
                    )
                    await message.reject()

        await consumer.wait_until_connected()
        prefetch = consumer.set_prefetch_count(1)
        declare = consumer.declare_queue(
            ("random_math_queue"),
        )
        await asyncio.gather(prefetch, declare)
        await consumer.start_consuming("random_math_queue", callback)

    async def produce(random_math_params, correlation_id, callback_queue_name, pool):
        publisher = AioClient(
            exchange="", client_name=f"PRODUCER_{correlation_id}", pool=pool
        )
        await publisher.wait_until_connected(timeout=CLIENT_ACTION_TIMEOUT)
        await publisher.declare_queue(
            "random_math_queue", timeout=CLIENT_ACTION_TIMEOUT
        )
        await publisher.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
        await publisher.publish_message(
            "random_math_queue",
            message=random_math_params,
            content_type="application/json",
            delivery_mode="not_persistent",
            correlation_id=correlation_id,
            reply_to=callback_queue_name,
        )

    pool = AioConnectionPool(n_connections=7)
    await pool.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
    client = AioClient(exchange="", client_name="Starter", pool=pool)
    target_info, dummy_info = rpc_infos.values()
    await client.wait_until_connected()
    await client.declare_queue(target_info["callback_queue_name"])
    asyncio.create_task(server_consume(pool))

    for i in range(len(target_info["correlation_ids"])):
        asyncio.create_task(
            client_consume(
                target_info["correlation_ids"][i],
                target_info["callback_queue_name"],
                pool,
            )
        )

    for info in (target_info, dummy_info):
        for i in range(len(info["correlation_ids"])):
            asyncio.create_task(
                produce(
                    info["random_math_params"][i],
                    info["correlation_ids"][i],
                    info["callback_queue_name"],
                    pool,
                )
            )
    try:
        await assert_fn_with_retry(target_info)
    finally:
        for queue in ("random_math_queue", target_info["callback_queue_name"]):
            await client.delete_queue(queue, if_unused=False, if_empty=False)
        await pool.close()
