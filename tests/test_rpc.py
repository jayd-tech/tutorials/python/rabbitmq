import io
import json
import random
import threading
import uuid

import pika
from tenacity import retry, stop_after_attempt, wait_fixed

from tutorial.client import get_rabbit_blocking_connection


def test_random_math_rpc(rpc_infos):
    in_memory_files = {}
    access_files_lock = threading.Lock()

    def assert_fn_with_retry(lookup_info):
        @retry(reraise=True, stop=stop_after_attempt(3), wait=wait_fixed(2))
        def assert_w_retry():
            for i, correlation_id in enumerate(lookup_info["correlation_ids"]):
                with access_files_lock:
                    content = in_memory_files[f"rpc_{correlation_id}.txt"].getvalue()
                assert content == str(
                    do_random_math(**lookup_info["random_math_params"][i])
                )

        assert_w_retry()

    def do_random_math(int_1, int_2, str_op):
        return eval(f"{int_1}{str_op}{int_2}")

    def server_consume():
        def callback(ch, method, properties, body):
            result = do_random_math(**json.loads(body))
            ch.basic_publish(
                exchange="",
                routing_key=properties.reply_to,
                properties=pika.BasicProperties(
                    correlation_id=properties.correlation_id,
                    content_type="text/plain",
                ),
                body=str(result),
            )

        channel = get_rabbit_blocking_connection().channel()
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(
            queue="random_math_queue", on_message_callback=callback, auto_ack=True
        )
        channel.start_consuming()

    def client_consume(correlation_id, callback_queue_name):
        def callback(ch, method, properties, body):
            if correlation_id == properties.correlation_id:
                with access_files_lock:
                    in_memory_files[f"rpc_{correlation_id}.txt"] = (f := io.StringIO())
                    f.write(body.decode("utf-8"))
                ch.basic_ack(delivery_tag=method.delivery_tag)
            else:
                ch.basic_reject(delivery_tag=method.delivery_tag)

        channel = get_rabbit_blocking_connection().channel()
        channel.basic_consume(queue=callback_queue_name, on_message_callback=callback)
        channel.start_consuming()

    def produce(random_math_params, correlation_id, callback_queue_name):
        channel = get_rabbit_blocking_connection().channel()
        channel.basic_publish(
            exchange="",
            routing_key="random_math_queue",
            properties=pika.BasicProperties(
                reply_to=callback_queue_name,
                correlation_id=correlation_id,
                content_type="application/json",
            ),
            body=json.dumps(random_math_params),
        )

    channel = get_rabbit_blocking_connection().channel()
    channel.queue_declare(queue="random_math_queue")
    # Make producers
    target_info, dummy_info = rpc_infos.values()
    for info in (target_info, dummy_info):
        channel.queue_declare(queue=info["callback_queue_name"])
        for i in range(2):
            threading.Thread(
                target=lambda: produce(
                    info["random_math_params"][i],
                    info["correlation_ids"][i],
                    info["callback_queue_name"],
                )
            ).start()

    # Make server consumer
    threading.Thread(target=server_consume, daemon=True).start()
    # Make client consumers
    for i in range(2):
        threading.Thread(
            target=lambda: client_consume(
                target_info["correlation_ids"][i], target_info["callback_queue_name"]
            ),
            daemon=True,
        ).start()

    try:
        assert_fn_with_retry(target_info)
    finally:
        channel.queue_delete("random_math_queue")
        for info in (target_info, dummy_info):
            channel.queue_delete(info["callback_queue_name"])
